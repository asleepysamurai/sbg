/**
 * SBG Entry Point
 */

const argv = require('minimist')(process.argv.slice(2));
const rows = argv.r;
const columns = argv.c;

function exitWithError(err) {
    console.error(err);
    process.exit(1);
};

if (!(rows && columns))
    exitWithError(new Error('Missing row or column. Specify -r and -c params.'));

const fillColor = argv.f || 'D8D9D8';
const spacerX = argv.x || 20;
const spacerY = argv.y || 20;
const size = argv.s || 100;

const radius = 20;
const hMove = size - (2 * radius);
const vMove = size - (2 * radius);

let rects = [];
let elems = [];

const startingRowId = argv.rp || 'A';
let startingColId = argv.cp ? parseInt(argv.cp) : NaN;
startingColId = isNaN(startingColId) ? 1 : startingColId;

function getRowNameFromNumber(num) {
    return String.fromCharCode(startingRowId.charCodeAt(0) + (num % 26));
};

for (let i = 0; i < rows; ++i) {
    rects[i] = rects[i] || [];
    for (let j = 0; j < columns; ++j) {
        rects[i][j] = [(j == 0 ? spacerX : rects[i][j - 1][0]) + (size + spacerX), (i == 0 ? spacerY : rects[i - 1][j][1]) + (size + spacerY)];
        elems.push(`<path d="M${rects[i][j][0]},${rects[i][j][1]} h${hMove} a${radius},${radius} 0 0 1 ${radius},${radius} v${vMove} a${radius},${radius} 0 0 1 -${radius},${radius} h-${hMove} a${radius},${radius} 0 0 1 -${radius},-${radius} v-${vMove} a${radius},${radius} 0 0 1 ${radius},-${radius} z" fill="#${fillColor}" id="Seat-${getRowNameFromNumber(i)}${j+startingColId}" />`);
    }
}

let svg = `<?xml version="1.0" encoding="utf-8"?>
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 800 700" enable-background="new 0 0 800 700" xml:space="preserve">
	${elems.join('\n')}
</svg>`;

console.log(svg);
