/**
 * Input
 * 	_ file
 * 	-r row starting prefix, default 'A'
 * 	-c column starting prefix, default 1
 * 	-i interchange row and column prefixes
 * 	--rr reverse row order
 * 	--rc reverse column order
 */

const argv = require('minimist')(process.argv.slice(2));
const promisify = require('util').promisify;
const path = require('path');
const svgson = promisify(require('svgson'));
const readFile = promisify(require('fs').readFile);
const writeFile = promisify(require('fs').writeFile);

function exitWithError(err) {
    console.error(err);
    process.exit(1);
};

async function getSVGPaths() {
    try {
        if (!argv._[0])
            throw new Error('Input filename not specified');

        const filePath = path.isAbsolute(argv._[0]) ? argv._[0] : path.resolve('./', argv._[0]);
        const svg = await readFile(filePath, { encoding: 'utf8' });
        try {
            await svgson(svg, {});
        } catch (jsonSVG) {
            let paths = [];
            (((jsonSVG || {}).childs) || []).forEach(item => {
                if (item.name == 'path')
                    paths.push(item.attrs);
            });

            return paths;
        }
    } catch (err) {
        exitWithError(err);
    }
};

function groupSVGPathsIntoRows(paths) {
    let group = [];
    let groupedPaths = [];
    let lastKnownStartCoords;

    paths.forEach(path => {
        if (!(path && path.d))
            return;

        const pathCoordsMatch = path.d.match(/^M([+-]?([0-9]*[.])?[0-9]+),([+-]?([0-9]*[.])?[0-9]+)/);
        if (!(pathCoordsMatch && pathCoordsMatch[1] && pathCoordsMatch[3]))
            exitWithError(new Error('Invalid path in svg'));

        const pathCoords = { x: parseFloat(pathCoordsMatch[1]), y: parseFloat(pathCoordsMatch[3]) };
        if (!(!isNaN(pathCoords.x) && !isNaN(pathCoords.y)))
            exitWithError(new Error('Invalid path coords in svg'));

        if (lastKnownStartCoords && pathCoords.y != lastKnownStartCoords.y) {
            if (group.length)
                groupedPaths.push(group);
            group = [];
        }

        group.push(path);
        lastKnownStartCoords = pathCoords;
    });

    return groupedPaths;
};

function getCharacterForNumber(num, isRow) {
    let startingChar = isRow ? (argv.r || 'A') : (parseInt(argv.c) || 1);

    if (argv.i)
        startingChar = isRow ? (parseInt(argv.r) || 1) : (argv.c || 'A');

    if (argv.n)
        startingChar = (parseInt(argv.i ? argv.r : argv.c) || 1);

    if (typeof startingChar == 'number')
        return `${num + startingChar}`;

    let chars = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    let startCharIndex = chars.indexOf(startingChar);
    if (startCharIndex == -1)
        exitWithError(new Error('Invalid starting prefix'));
    let preChars = chars.splice(0, startCharIndex);
    chars = chars.concat(preChars);

    return chars[num % chars.length];
};

function giveGroupedPathsIds(groupedPaths) {
    for (let _r = 0, r = (argv.rr ? groupedPaths.length - 1 : 0); argv.rr ? (r >= 0) : (r < groupedPaths.length);
        ++_r && (argv.rr ? --r : ++r)) {
        for (let _c = 0, c = (argv.rc ? groupedPaths[r].length - 1 : 0); argv.rc ? (c >= 0) : (c < groupedPaths[r].length);
            ++_c && (argv.rc ? --c : ++c)) {
            groupedPaths[r][c].id = argv.i ?
                (getCharacterForNumber(_c) + '-' + getCharacterForNumber(_r, true)) :
                (getCharacterForNumber(_r, true) + '-' + getCharacterForNumber(_c));
            groupedPaths[r][c].id = `${argv.p || 'Seat-'}${groupedPaths[r][c].id}`;
        }
    }
};

function buildSVG(groupedPaths) {
    let elems = [];

    for (let r = 0; r < groupedPaths.length; ++r) {
        for (let c = 0; c < groupedPaths[r].length; ++c) {
            let attrs = [];
            Object.keys(groupedPaths[r][c]).forEach(key => {
                attrs.push(`${key}="${groupedPaths[r][c][key]}"`);
            });

            elems.push(`<path ${attrs.join(' ')} />`);
        }
    }

    const svg = `<?xml version="1.0" encoding="utf-8"?>
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 800 700" enable-background="new 0 0 800 700" xml:space="preserve" style="transform:scale(10)">
	${elems.join('\n')}
</svg>`;

    return svg;
};

async function writeSVG(svg) {
    try {
        if (!argv._[1])
            throw new Error('Output filename not specified');

        const filePath = path.isAbsolute(argv._[1]) ? argv._[1] : path.resolve('./', argv._[1]);
        writeFile(filePath, svg);
    } catch (err) {
        exitWithError(err);
    }
};

async function run() {
    const paths = await getSVGPaths();
    const groupedPaths = groupSVGPathsIntoRows(paths);

    giveGroupedPathsIds(groupedPaths);
    const svg = buildSVG(groupedPaths);
    await writeSVG(svg);
};

run();
